#!/usr/bin/python

import sys
import datetime
import csv
from calendar import monthrange

reload(sys)
sys.setdefaultencoding('utf-8')

import urllib2, urllib, socket, time
import gzip, StringIO
import re, random, types

from bs4 import BeautifulSoup

base_url = "https://www.lawinsider.com"

user_agents = list()


# results from the search engine
# basically include url, title,content
class SearchResult:
    def __init__(self):
        self.url = ''
        self.title = ''
        self.content = ''

    def getURL(self):
        return self.url

    def setURL(self, url):
        self.url = url

    def getTitle(self):
        return self.title

    def setTitle(self, title):
        self.title = title

    def getContent(self):
        return self.content

    def setContent(self, content):
        self.content = content

    def printIt(self, prefix=''):
        print 'url\t->', self.url
        print 'title\t->', self.title
        print 'content\t->', self.content
        print

    def writeFile(self, filename):
        file = open(filename, 'a')
        try:
            file.write('url:' + self.url + '\n')
            file.write('title:' + self.title + '\n')
            file.write('content:' + self.content + '\n\n')

        except IOError, e:
            print 'file error:', e
        finally:
            file.close()


class SearchAPI:
    def __init__(self):
        timeout = 40
        socket.setdefaulttimeout(timeout)

    def randomSleep(self):
        sleeptime = random.randint(60, 120)
        time.sleep(sleeptime)


    def extractSearchResults(self, html):

        print "Extracting links from html..."

        results = list()

        soup = BeautifulSoup(html, 'html.parser')

        for link in soup.find_all('a', attrs={'data-search-link': 'true'}):
            href = base_url + str(link.get('href'))
            print "Found link: ", href

            results.append(href)

        return results


    # search web
    # @param sturt_offset_numb
    # @param num -> number of search results to return
    def search(self, start_offset, end_offset, date):

        search_results = list()

        for current_offset in xrange(start_offset, end_offset):

            scraped_links_file = open('./scraped_urls.txt', 'a')

            url = base_url + "/search?exhibit_id=&filing_type=&offset=" + str(start_offset) + "&query=" + date + "&tags="

            print "Url -> ", url
            print >> scraped_links_file, url

            retry = 5
            while (retry > 0):
                try:
                   #  request = urllib2.Request(url)
                   #
                   #  length = len(user_agents)
                   #  index = random.randint(0, length - 1)
                   #  user_agent = user_agents[index]
                   # # request.add_header('User-agent', user_agent)
                   #  request.add_header('connection', 'keep-alive')
                   #  request.add_header('Accept-Encoding', 'gzip')
                   #  request.add_header('referer', base_url)

                    print "Send request..."
                    response = urllib.urlopen(url)

                    html = response.read()
                    #
                    # if (response.headers.get('content-encoding', None) == 'gzip'):
                    #     print "gziping..."
                    #     html = gzip.GzipFile(fileobj=StringIO.StringIO(html)).read()

                    results = self.extractSearchResults(html)

                    if not results:
                        return results

                    search_results.append(results)

                    save_result_to_csv_file(results)

                    start_offset += 10

                    break

                except urllib2.URLError, e:
                    print 'url error:', e
                    self.randomSleep()
                    retry = retry - 1
                    print >> scraped_links_file, "FAILED"
                    continue

                except Exception, e:
                    print 'error:', e
                    retry = retry - 1
                    self.randomSleep()
                    print >> scraped_links_file, "FAILED"
                    continue

            if retry <= 0:
                print "Finishing loop with retry <= 0"
                return False

        scraped_links_file.close()

        return True




def save_result_to_csv_file(result_list):
    with open('./result.csv', 'a') as csvfile:
        csv_writer = csv.writer(csvfile, delimiter = '\n', dialect='excel')

        # for href in result_list:
        csv_writer.writerow(result_list)


def load_user_agent():
    fp = open('./user_agents.txt', 'r')

    line = fp.readline().strip('\n')

    while (line):
        user_agents.append(line)
        line = fp.readline().strip('\n')

    fp.close()


def crawl_by_month():
    # example of date part of query "filing_date%3A%5B1899-12-31+TO+2016-10-18%5D"
    # start date:  1994-1-4

    print "Started crawling by month ..."

    # Load use agent string from file
    load_user_agent()

    # Create a SearchAPI instance
    api = SearchAPI()

    # set expect search results to be crawled
    start_offset = 0
    end_offset = 9980

    current_year = datetime.datetime.now().year
    current_month = datetime.datetime.now().month

    start_year = 1994
    start_month = 1
    start_day = 4

    for year in xrange(start_year, current_year):

        for month in xrange(start_month, 13):

            next_year = year

            if month == 12:
                next_month = 1
            else:
                next_month = month + 1

            if next_month == 12:
                next_year = year + 1

            date_query = "filing_date%3A%5B" + str(year) + "-" + str(month) + "-" + str(start_day) + "+TO+" \
                         + str(next_year) + "-" + str(next_month) + "-" + str(start_day) + "%5D"

            print "Search links for date: ",month,":",year,"-",next_month,":",next_year,"."
            print "Date query part: ", date_query

            api.search(start_offset, end_offset, date_query)


def crawl_by_10_days():
    # example of date part of query "filing_date%3A%5B1899-12-31+TO+2016-10-18%5D"
    # start date:  1994-1-4

    print "Started crawling by month ..."

    # Load use agent string from file
    load_user_agent()

    # Create a SearchAPI instance
    api = SearchAPI()

    # set expect search results to be crawled
    start_offset = 0
    end_offset = 9980

    current_year = datetime.datetime.now().year
    start_year = 2005
    start_month = 1

    for year in xrange(start_year, current_year):

        for month in xrange(start_month, 12):

            last_day_in_month = monthrange(year, month)[1]

            for day in xrange(1, 40, 10):
                start_day = day
                print "start_day ", start_day

                end_day = day + 10
                print "end_day ", end_day

                if end_day > last_day_in_month:
                    end_day = last_day_in_month

                if end_day == start_day:
                    break

                date_query = "filing_date%3A%5B" + str(year) + "-" + str(month) + "-" + str(start_day) + "+TO+" \
                             + str(year) + "-" + str(month) + "-" + str(end_day) + "%5D"

                print "Date query part: ", date_query

                api.search(start_offset, end_offset, date_query)

                if end_day == last_day_in_month:
                    break




def crawler():

    # Load use agent string from file
    load_user_agent()

    # Create a SearchAPI instance
    api = SearchAPI()

    # set expect search results to be crawled
    expect_num = 109320 # results found 1093200
    offset_num = 9990

    print "Expected number of urls crawled ", offset_num

    api.search(offset_num, expect_num)
    # save_result_to_csv_file(results)
    # for r in results:
    #     r.printIt()



if __name__ == '__main__':
    print "[", datetime.datetime.now().time(), "] Starting crawler."

    # crawl_by_month()

    crawl_by_10_days()

    print "[", datetime.datetime.now().time(), "] Crawler stoped."