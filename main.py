
'''
# parse http://chroniclingamerica.loc.gov/newspapers/
# save all urls to txt file
# go through 100 urls, load page with big version of image, download image
'''


from bs4 import BeautifulSoup
import urllib
import datetime

print "Scraper strted ", datetime.datetime.now().time()

url = 'http://chroniclingamerica.loc.gov'

try:

    response = urllib.urlopen(url + "/newspapers/")
    print "Downloading ", url, "newspapers/ ..."

    main_html_page = response.read()
    soup = BeautifulSoup(main_html_page, 'html.parser')

    count = 0

    for link in soup.find_all('a'):
        #print "Parsing ", link, "..."

        if link.text == "Yes" and count < 100:
            count = count + 1
            print "Found link ", link.get('href')

            child_url = url + link.get('href')
            child_url_responce = urllib.urlopen(child_url)
            child_html_page = child_url_responce.read()

            print "Parsing url ", child_url, "..."
            child_soup = BeautifulSoup(child_html_page, 'html.parser')

            for link in child_soup.find_all('img', attrs={'class':'thumbnail'}):

                image_page_link = link.get('src')
                image_link = url + image_page_link[:-14] + ".jp2"

                print "Found image page link", image_link, ". Go to image page."
                urllib.urlretrieve(image_link, "/Users/iram/Workspace/Upwork/Scraper/Storage/" + str(count) + ".jpg")

        elif  count > 100:
            break

    print "Scraper stoped ", datetime.datetime.now().time()

except Exception as e:
    print(str(e))