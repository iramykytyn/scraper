from bs4 import BeautifulSoup
import urllib2, sys
import gzip, StringIO
import random

reload(sys)
sys.setdefaultencoding('utf-8')

base_url = "https://www.lawinsider.com"
user_agents = list()

class ExtractData:

    def __init__(self, url):
        self.url = url
        self.title = ''
        self.date = ''
        self.content = ''
        self.tags = set()
        self.right_side_tags = ()


    def getURL(self):
        return self.url

    def setURL(self, url):
        self.url = url

    def getTitle(self):
        return self.title

    def setTitle(self, title):
        self.title = title

    def getDate(self):
        return self.date

    def setDate(self, date):
        self.date = date

    def getContent(self):
        return self.content

    def setContent(self, content):
        self.content = content

    def getTags(self):
        return self.tags

    def setTags(self, tags):
        self.tags = tags

    def getRTags(self):
        return self.right_side_tags

    def setRTags(self, tags):
        self.right_side_tags = tags


    def GetHtml(self):

        scraped_links_file = open('./failed_to_parse.txt', 'a')

        print "Getting html from ", self.url
        print >> scraped_links_file, self.url

        retry = 5
        while retry > 0 :
            try:
                request = urllib2.Request(self.url)

                length = len(user_agents)
                index = random.randint(0, length - 1)
                user_agent = user_agents[index]
                request.add_header('User-agent', user_agent)
                request.add_header('connection', 'keep-alive')
                request.add_header('Accept-Encoding', 'gzip')
                request.add_header('referer', base_url)

                print "Send request..."
                response = urllib2.urlopen(request)

                html = response.read()

                if (response.headers.get('content-encoding', None) == 'gzip'):
                    print "gziping..."
                    html = gzip.GzipFile(fileobj=StringIO.StringIO(html)).read()

                return html

                break

            except urllib2.URLError, e:
                print 'url error:', e
                self.randomSleep()
                retry = retry - 1
                print >> scraped_links_file, "FAILED"
                continue

            except Exception, e:
                print 'error:', e
                retry = retry - 1
                self.randomSleep()
                print >> scraped_links_file, "FAILED"
                continue



    def ParsePage(self):
        print "Parsing url: ", self.url

        html = self.GetHtml()

        if not html:
            print "Error. Het empty html for url ", self.url

        soup = BeautifulSoup(html, 'html.parser')

        title = soup.title.string
        self.setTitle(title)

        for tags in soup.find_all('a', attrs={'class': 'tag'}):
            tag = tags.string
            self.tags.add(tag)

            print "Found tag: ", tag


        for sidebar_box in soup.find_all('div', attrs={'class': 'sidebar'}):

            if sidebar_box.find('h4').getText() == 'Items found in this contract':

                tags = set()

                for sub_category in sidebar_box.find_all('a'):

                    sub_cat_text = sub_category.getText()

                    if not sub_cat_text:
                        continue

                    print "Sub category: ", sub_cat_text

                    tags.add(sub_cat_text)


                self.setRTags(tags)



                # for category in sidebar_box.find_all('h5'):
                #
                #     print "Category ", category.getText()
                #
                #     if not category.getText():
                #         continue
                #
                #     tags_set = set()
                #
                #     for sub_category in sidebar_box.find_all('a'):
                #
                #         sub_cat_text = sub_category.getText()
                #
                #         if not sub_cat_text:
                #             continue
                #
                #         print "Sub category: ", sub_cat_text
                #         tags_set.add(sub_cat_text)
                #
                # self.setRTags(category.getText(), tags_set)

        date = soup.find('span', attrs={'class': 'date'})
        self.setDate(date.getText())

        content = soup.find('div', attrs={'class': 'row contract-content'})
        self.setContent(content.prettify())


def load_user_agent():
    fp = open('./user_agents.txt', 'r')

    line = fp.readline().strip('\n')

    while (line):
        user_agents.append(line)
        line = fp.readline().strip('\n')

    fp.close()



load_user_agent()

testUrl = 'https://www.lawinsider.com/contracts/4i7bKZvxua8FNIUVytKxXe/invitrogen/agreement/2005-02-14'

testExtractor = ExtractData(testUrl)

testExtractor.ParsePage()

print "Title: ", testExtractor.getTitle()
print "Tags: ", testExtractor.getTags()
print "Right tags: ", testExtractor.getRTags()

print "Date: ", testExtractor.getDate()

testFile = open("test.txt", 'w')


content = testExtractor.getContent()

print content

testFile.write(str(content))

if not content:
    testFile.write(content)
else:
    print "Empty content for url ", testExtractor.getURL()

